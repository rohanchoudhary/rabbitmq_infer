The following repository contains the following files :

1) producer.py : 
This script basically takes the input path of the desired video file and encodes each frame and sends it over the rabbitMQ amqp protocol to the consumer.

2) consumer_infer.py : 
This script is responsible for recieving the encoded frames, decoding them back to their original format and produce inference on it using REST or gRPC. It also prints the average FPS for all the frames infered. It expects several arguments like inference_mode, port, serving_model_name, version, size, model_input_label and model_output_label. You need to enter all these arguments to use the file accordingly.

Although, the defaults are set to work for gRPC on port 8500 using model retinaface_mbv2 with image input size as [224,224] ; version 1 ; model_input_label as input_image and model_output_label as tf_op_layer_GatherV2_3 .

3) test.mp4 :
This is the sample video used to produce inference on.

4) RabbitMQ_tfserving.pdf :
This is a documentaion file which contains general information about rabbitMQ, tfServing, producing and consuming scripts.
