import pika
import argparse
import tensorflow as tf
import json
import requests
import cv2
import numpy as np
import time
import grpc
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc

parser = argparse.ArgumentParser(description = 'arguments for served model')
parser.add_argument('--inference_mode',type = str, help = 'output path', default = "gRPC")
parser.add_argument('--port',type = str, help = 'output path', default = "8500")
parser.add_argument('--serving_model_name',type = str, help = 'model name', default = "retinaface_mbv2")
parser.add_argument('--version',type = int, help = 'version number', default = 1)
parser.add_argument('--size',type = list, help = 'input size of image', default = [224,224])
parser.add_argument('--model_input_label',type = str, help = 'Model Input label', default = "input_image" )
parser.add_argument('--model_output_label',type = str, help = 'Model Output label', default = "tf_op_layer_GatherV2_3")

args = parser.parse_args()

inference_mode = args.inference_mode
port = args.port
serving_model_name= args.serving_model_name
version=args.version
size=args.size
input_label=args.model_input_label
output_label=args.model_output_label

#establishing connection to rabbitMQ server
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='infer')	#declaring the queue 

class infer_frame():
    t=0
    counter=0
    def infer_gRPC(ch, method, properties, body):
        #decoding the recieved encoded frame:
        print(" [x] Received frame ") 
        nparr = np.frombuffer(body, np.uint8)          #np.fromstring 
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
		
        #initializing neccessary gRPC variables:
        channel = grpc.insecure_channel('localhost:' + str(port))     		#port number for gRPC
        stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
        grpc_request = predict_pb2.PredictRequest()
        grpc_request.model_spec.name = serving_model_name   				#name of the model
        grpc_request.model_spec.signature_name = 'serving_default'
        grpc_request.model_spec.version.value = version						#version
        width = size[0]
        height = size[1]
		
		#inference :
        image = cv2.resize(image, (int(width),int(height)))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = image.astype(np.float32)
        image = np.expand_dims(image, axis = 0)
        grpc_request.inputs[input_label].CopyFrom(tf.make_tensor_proto(image, shape=image.shape))
        a=time.time()
        result = stub.Predict(grpc_request,10)
        b=time.time()
        print("for this frame FPS : ", 1/(b-a))
        result = result.outputs[output_label]
        print(result)
		
        #average FPS for all frames:
        infer_frame.t +=((b-a))
        infer_frame.counter+=1
        print("total time : ",infer_frame.t)
        print("total frames : ",infer_frame.counter)
        print("Average FPS : ",1/(infer_frame.t/infer_frame.counter),"\n")

    def infer_REST(ch, method, properties, body):
	    #decoding the recieved encoded frame:
        print(" [x] Received frame ") 
        nparr = np.frombuffer(body, np.uint8) 
        image = cv2.imdecode(nparr, cv2.IMREAD_COLOR)
		
        #initializing neccessary REST variables:
        width = size[0]
        height = size[1]
        url_link = 'http://localhost:' + str(port) + '/v1/models/' + serving_model_name + '/versions/' + str(version) + ':predict'
        
		#inference
        image = cv2.resize(image, (int(width),int(height)))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        image = image.astype(np.uint8)
        image = np.expand_dims(image, axis = 0)
        data = json.dumps({"signature_name": "serving_default", "instances": image.tolist()})
        headers = {"content-type": "application/json"}
        start=time.time()
        json_response = requests.post(url_link, data=data, headers=headers)
        end=time.time()
        predictions = json.loads(json_response.text)['predictions']
        print("for this frame FPS : ", 1/(end-start))
        print(predictions)   
		
        #average FPS for all frames recieved:
        infer_frame.t+=((end-start))
        infer_frame.counter+=1
        print("total time : ",infer_frame.t)
        print("total frames : ",infer_frame.counter)
        print("Average FPS : ",1/(infer_frame.t/infer_frame.counter),"\n")		

if inference_mode == "gRPC":
    channel.basic_consume(
        queue='infer', on_message_callback=infer_frame.infer_gRPC, auto_ack=True)
elif inference_mode == "REST":
    channel.basic_consume(
        queue='infer', on_message_callback=infer_frame.infer_REST, auto_ack=True)
else:
    print("Please select Valid Inference Type. Options: 1) REST 2) gRPC ")
    exit()
	
print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()

