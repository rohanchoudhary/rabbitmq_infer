import pika
import cv2

#establishing connection to rabbitMQ server:
connection = pika.BlockingConnection(
    pika.ConnectionParameters(host='localhost'))
channel = connection.channel()

channel.queue_declare(queue='infer')		#declaring the queue 

vid = cv2.VideoCapture('test.mp4')			#input_video_location

i=0
while(True):
	ret, frame = vid.read()
	image = cv2.imencode('.jpg',frame)[1].tostring()						#encoding the frame
	channel.basic_publish(exchange='', routing_key='infer', body=image)		#sendign the encoded frame
	i+=1
	print(" Sent frame ", i)
vid.release()

connection.close()